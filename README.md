
# Timeline

Trivial test app briefly displaying list of entries of [tjournal](https://tjournal.ru/) timeline

Supports only text messages and static images information. Other message(e.g twitter) and media(e.g. videos, gifs) types are not supported.

- Pattern: MVP
- Layout: programmatically(Snapkit)
- Network: URLSession
- Deserialization: SwiftyJSON
- Image asynchronous loading and caching used ([based on this](https://developer.apple.com/documentation/uikit/views_and_controls/table_views/asynchronously_loading_images_into_table_and_collection_views))

<img src="screenshots/1.png" alt="screenshot1" width="200" height="450">
<img src="screenshots/2.png" alt="screenshot2" width="200" height="450">
<img src="screenshots/3.png" alt="screenshot3" width="200" height="450">
<img src="screenshots/4.png" alt="screenshot4" width="200" height="450">

Futher development ways

- Prefetching
- Support of all media types
- Reload timeline
