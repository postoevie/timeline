//
//  TimelinePresenter.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

import Foundation

class TimelinePresenter: TimelineViewPresenterProtocol  {
    
    var fetchingDataViewCompletion: VoidClosure?
    
    private var cellData = [TimelineCellData]()
    
    // if several possible datasources exist, make protocol
    private let dataSource = TimelineDataSource()
    
    func fetchData() {
        dataSource.fetchData() { [weak self] timelineData in
            self?.cellData = timelineData
            self?.fetchingDataViewCompletion?()
        }
    }
    
    func fetchImage(url: URL, completion: @escaping ImageClosure) {
        dataSource.fetchImage(url: url, completion: completion)
    }
    
    func getData(for indexPath: IndexPath) -> TimelineCellData {
        return cellData[indexPath.section]
    }
    
    func numberOfSections() -> Int {
        cellData.count
    }
}
