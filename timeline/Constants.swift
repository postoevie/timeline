//
//  Constants.swift
//  timeline
//
//  Created by Igor Postoev on 30.12.2021.
//

struct Constants {
    
    struct View {
        static let cellOffset = 10
    }
    
    struct Web {
        static let timelineUrlString = "https://api.tjournal.ru/v2.1/timeline"
        static let osnovaUrlString = "https://leonardo.osnova.io"
    }
}
