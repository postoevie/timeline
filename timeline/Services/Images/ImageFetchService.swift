//
//  ImageFetchService.swift
//  timeline
//
//  Created by Igor Postoev on 05.01.2022.
//

class ImageFetchService {
    
    private var cache = NSCache<NSURL, UIImage>()
    private var completionListsByImageUrls = [NSURL: [ImageClosure]]()
    private var session = URLSession(configuration: .default)
    
    func fetchImage(url: NSURL, completion: @escaping ImageClosure) {
        
        if let image = getCachedImage(url: url) {
            return completion(image)
        }
        
        if var closureList = completionListsByImageUrls[url] {
            closureList.append(completion)
            completionListsByImageUrls[url] = closureList
            return
        } else {
            completionListsByImageUrls[url] = [completion]
        }
        
        session.dataTask(with: url as URL) { [weak self] data, response, error in
            guard let fetchedResponse = response as? HTTPURLResponse,
                  fetchedResponse.statusCode == 200,
                  let fetchedData = data,
                  let image = UIImage(data: fetchedData) else {
                      //пройти по всем и вернуть nil/placeholder
                      self?.completionListsByImageUrls[url] = nil
                      return
                  }
            
            // add to cache
            self?.cache.setObject(image, forKey: url)
            
            // reply to all completions
            DispatchQueue.main.async {
                self?.completionListsByImageUrls[url]?.forEach { completion in
                    completion(image)
                    
                }
                self?.completionListsByImageUrls[url] = nil
            }
        }.resume()
    }
    
    private func getCachedImage(url: NSURL) -> UIImage? {
        return cache.object(forKey: url)
    }
}
