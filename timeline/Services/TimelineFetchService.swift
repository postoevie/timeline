//
//  TimelineFetchService.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

import Foundation

class TimelineFetchService {
    
    private let session = URLSession(configuration: .default)
    private var dataTask: URLSessionDataTask?
    
    func fetchData(completion: DataClosure?) {
        
        dataTask?.cancel()
        
        guard let url = URL(string: Constants.Web.timelineUrlString) else {
            return
        }
        
        dataTask = session.dataTask(with: url) { [weak self] data, response, error in
            defer {
                self?.dataTask = nil
            }
            guard let fetchedData = data,
                  let fetchedResponse = response as? HTTPURLResponse,
                  fetchedResponse.statusCode == 200 else {
                      return
            }
            DispatchQueue.main.async {
                completion?(fetchedData)
            }
        }
        
        dataTask?.resume()
    }
}
