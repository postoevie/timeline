//
//  ViewController.swift
//  timeline
//
//  Created by Igor Postoev on 29.12.2021.
//

import UIKit

class TimelineViewController: UIViewController,
                              UITableViewDelegate,
                              UITableViewDataSource,
                              TimelineCellDelegate {
    
    var presenter: TimelineViewPresenterProtocol!
    
    private let tableView = UITableView()
    private let foregroundView = TimelimeForegroundView()
    private let indicatorView = UIActivityIndicatorView()
    
    private let cellPlaceholderImage = UIImage(named: "placeholder")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(foregroundView)
        view.addSubview(tableView)
        view.addSubview(indicatorView)
        setupSubviews()
        setupConstraints()
    }
    
    private func setupSubviews() {
        foregroundView.touchOutsideAreaCompletion = { [weak self] in
            if let table = self?.tableView {
                self?.view.bringSubviewToFront(table)
            }
        }
        setupTableView()
        indicatorView.style = .large
    }
    
    private func setupTableView() {
        tableView.backgroundColor = .xLightGray
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TimelineCell.self,
                           forCellReuseIdentifier: String(describing: TimelineCell.self))
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        foregroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        indicatorView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    func reload() {
        tableView.reloadData()
    }
    
    func setLoading(state: Bool) {
        state ? indicatorView.startAnimating() : indicatorView.stopAnimating()
    }
    
    //MARK: -UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    //MARK: -UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: TimelineCell.self)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier,
                                                       for: indexPath) as? TimelineCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        let data = presenter.getData(for: indexPath)
        cell.update(with: data)
        
        if let avatarUrl = data.headerData.avatarUrl {
            cell.setAvatar(with: avatarUrl, cellPlaceholderImage)
            presenter.fetchImage(url: avatarUrl) { [weak cell] image in
                guard let unImage = image else {
                    return
                }
                cell?.updateAvatar(with: avatarUrl, unImage)
            }
        }
        if let contentImageUrl = data.imageUrl {
            cell.setContent(with: contentImageUrl, cellPlaceholderImage)
            presenter.fetchImage(url: contentImageUrl) { [weak cell, weak self] image in
                guard let unImage = image else {
                    return
                }
                self?.tableView.beginUpdates()
                cell?.updateContent(with: contentImageUrl, unImage)
                self?.tableView.endUpdates()
            }
        }
        
        return cell
    }
    
    //MARK: -TimelineCellDelegate
    
    func handleTapped(image: UIImage) {
        view.bringSubviewToFront(foregroundView)
        foregroundView.set(image: image)
        foregroundView.show()
    }
}
