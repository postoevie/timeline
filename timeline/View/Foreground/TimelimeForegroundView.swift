//
//  ForegroundImageView.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

import CoreGraphics

class TimelimeForegroundView: UIView {
    
    var touchOutsideAreaCompletion: VoidClosure?
    private let backgroundView = UIView()
    private let imageView = UIImageView()
    
    init() {
        super.init(frame: CGRect())
        backgroundColor = .clear
        addSubview(backgroundView)
        addSubview(imageView)
        setupSubviews()
    }
    
    private func setupSubviews() {
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0
        let recognizer = UITapGestureRecognizer(target: self,
                                                action: #selector(backgroundTapped))
        backgroundView.addGestureRecognizer(recognizer)
        
        imageView.contentMode = .scaleAspectFill
        
        backgroundView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        imageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
    }
    
    @objc
    private func backgroundTapped() {
        clear()
        hide()
        touchOutsideAreaCompletion?()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(image: UIImage) {
        imageView.image = image
    }
    
    func clear() {
        imageView.image = nil
    }
    
    func show() {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: { [weak self] in
            self?.backgroundView.alpha = 0.7
        })
    }
    
    func hide() {
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: { [weak self] in
            self?.backgroundView.alpha = 0
        })
    }
}
