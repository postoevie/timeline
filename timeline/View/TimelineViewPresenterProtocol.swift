//
//  TimelineViewPresenterProtocol.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

protocol TimelineViewPresenterProtocol {
    
    func getData(for indexPath: IndexPath) -> TimelineCellData
    func numberOfSections() -> Int
    func fetchImage(url: URL, completion: @escaping ImageClosure)
}
