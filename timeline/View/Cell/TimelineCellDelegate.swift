//
//  TimelineCellDelegate.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

import UIKit


protocol TimelineCellDelegate: UIViewController {
    
    func handleTapped(image: UIImage)
}
