//
//  TimelineCellFooter.swift
//  timeline
//
//  Created by Igor Postoev on 30.12.2021.
//

struct TimelineCellFooterData {
    
    var commentNumber: Int?
    var ratingNumber: Int?
}
