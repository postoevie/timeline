//
//  TimeineCellFooter.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

class TimeineCellFooter: UIView {
    
    private let stackView = UIStackView()
    private let commentImageView = UIImageView()
    private let commentsLabel = UILabel()
    private let ratingLabel = UILabel()
    
    private let avatarViewSideSize = 30
    private let labelsFontSize = CGFloat(14)
    
    init() {
        super.init(frame: CGRect())
        addSubview(stackView)
        
        setupSubviews()
        setupConstraints()
    }
    
    private func setupSubviews() {
        setupStackView()
        commentsLabel.font = UIFont.systemFont(ofSize: labelsFontSize)
        ratingLabel.font = UIFont.systemFont(ofSize: labelsFontSize)
        ratingLabel.textColor = .darkGreen
    }
    
    
    private func setupStackView() {
        stackView.spacing = 10
        stackView.addArrangedSubview(commentImageView)
        stackView.addArrangedSubview(commentsLabel)
        stackView.addArrangedSubview(UIView())
        stackView.addArrangedSubview(ratingLabel)
    }
   
    func setupConstraints() {
        let offset = Constants.View.cellOffset
        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(offset)
            make.trailing.equalToSuperview().offset(-offset)
        }
        commentImageView.snp.makeConstraints { make in
            make.height.equalTo(avatarViewSideSize)
            make.width.equalTo(avatarViewSideSize)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with data: TimelineCellFooterData) {
        let image = UIImage(systemName: "message")?.withHorizontallyFlippedOrientation()
        commentImageView.image = image?.withRenderingMode(.alwaysTemplate)
        commentImageView.tintColor  = .lightGray
        
        commentsLabel.text = data.commentNumber?.formatWithLetters()
        ratingLabel.text = String(data.ratingNumber ?? 0)
    }
}
