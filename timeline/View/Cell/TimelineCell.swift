//
//  TimelineCell.swift
//  timeline
//
//  Created by Igor Postoev on 30.12.2021.
//

class TimelineCell: UITableViewCell {
    
    weak var delegate: TimelineCellDelegate?
    
    private let header = TimelineCellHeader()
    private let imageContentView = UIImageView()
    private let titlesView = TimelineCellTitlesView()
    private let footer = TimeineCellFooter()
    
    private let stackView = UIStackView()
    
    private var contentImageUrl: URL?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(stackView)
        setupSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews() {
        setupImageView()
        setupStackView()
    }
    
    private func setupImageView() {
        let imageTapRecognized = UITapGestureRecognizer(target: self,
                                                        action: #selector(imageTapped(_:)))
        imageContentView.addGestureRecognizer(imageTapRecognized)
        imageContentView.isUserInteractionEnabled = true
    }
    
    @objc
    private func imageTapped(_ sender: UIGestureRecognizer) {
        if let imageView = sender.view as? UIImageView,
           let image = imageView.image {
            delegate?.handleTapped(image: image)
        }
    }
    
    private func setupStackView() {
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.addArrangedSubview(header)
        stackView.addArrangedSubview(titlesView)
        stackView.addArrangedSubview(imageContentView)
        stackView.addArrangedSubview(footer)
    }
    
    private func setupConstraints() {
        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
            make.trailing.leading.equalToSuperview()
        }
    }
    
    func update(with data: TimelineCellData) {
        header.update(with: data.headerData)
        titlesView.update(title: data.title,
                          subtitle: data.subtitle)
        footer.update(with: data.footerData)
        
        // изображения определяются в отдельном порядке
        contentImageUrl = nil
        imageContentView.image = nil
    }
    
    func setContent(with url: URL, _ placeholderImage: UIImage) {
        contentImageUrl = url
        imageContentView.image = placeholderImage.scaledToFit(width: contentView.bounds.width)
    }
    
    func updateContent(with url: URL, _ image: UIImage) {
        // т к изображение может быть получено асинхронно, проверяется, соответствет ли оно текущему url ячейки
        guard contentImageUrl == url else {
            return
        }
        imageContentView.image = image.scaledToFit(width: contentView.bounds.width)
        stackView.layoutSubviews()
    }
    
    func setAvatar(with url: URL, _ placeholderImage: UIImage?) {
        header.setAvatar(with: url, placeholderImage)
    }
    
    func updateAvatar(with url: URL, _ image: UIImage?) {
        header.updateAvatar(with: url, image)
    }
}
