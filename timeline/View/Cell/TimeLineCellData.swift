//
//  TimeLineCellData.swift
//  timeline
//
//  Created by Igor Postoev on 30.12.2021.
//

struct TimelineCellData {
    
    var headerData: TimeLineCellHeaderData
    var imageUrl: URL?
    var title: String?
    var subtitle: String?
    var footerData: TimelineCellFooterData
}
