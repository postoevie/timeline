//
//  TimelineCellHeader.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

class TimelineCellHeader: UIView {
    
    private let stackView = UIStackView()
    private let subsiteAvatarView = UIImageView()
    private let subsiteNameLabel = UILabel()
    private let authorNameLabel = UILabel()
    private let creationTimeLabel = UILabel()
    
    private let avatarViewSideSize = 30
    private let labelsFontSize = CGFloat(14)
    
    private var avatarImageUrl: URL?
    
    init() {
        super.init(frame: CGRect())
        addSubview(stackView)
        
        setupSubviews()
        setupConstraints()
    }
    
    private func setupSubviews() {
        setupStackView()
        subsiteAvatarView.layer.cornerRadius = 8
        subsiteAvatarView.layer.masksToBounds = true
        subsiteNameLabel.font = .systemFont(ofSize: labelsFontSize, weight: .bold)
        authorNameLabel.font = .systemFont(ofSize: labelsFontSize)
        creationTimeLabel.font = .systemFont(ofSize: labelsFontSize)
        let priorityLessThanOther = UILayoutPriority(749)
        creationTimeLabel.setContentCompressionResistancePriority(priorityLessThanOther,
                                                                  for: .horizontal)
    }
    
    private func setupStackView() {
        stackView.addArrangedSubview(subsiteAvatarView)
        stackView.addArrangedSubview(subsiteNameLabel)
        stackView.addArrangedSubview(authorNameLabel)
        stackView.addArrangedSubview(creationTimeLabel)
        stackView.addArrangedSubview(UIView())
        stackView.spacing = 10
    }
   
    func setupConstraints() {
        let offset = Constants.View.cellOffset
        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(offset)
            make.trailing.equalToSuperview().offset(-offset)
        }
        subsiteAvatarView.snp.makeConstraints { make in
            make.height.equalTo(avatarViewSideSize)
            make.width.equalTo(avatarViewSideSize)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with data: TimeLineCellHeaderData) {
        subsiteNameLabel.text = data.subsiteName
        authorNameLabel.text = data.authorName
        creationTimeLabel.text = data.creationTimeInterval?.formatWithRelativeTime()
        avatarImageUrl = nil
        subsiteAvatarView.image = nil
    }
    
    func setAvatar(with url: URL, _ placeholderImage: UIImage?) {
        avatarImageUrl = url
        subsiteAvatarView.image = placeholderImage
    }
    
    func updateAvatar(with url: URL, _ image: UIImage?) {
        guard avatarImageUrl == url else {
            return
        }
        subsiteAvatarView.image = image
    }
}
