//
//  TimeLineCellHeaderData.swift
//  timeline
//
//  Created by Igor Postoev on 30.12.2021.
//

struct TimeLineCellHeaderData {
    
    var avatarUrl: URL?
    var subsiteName: String?
    var authorName: String?
    var creationTimeInterval: TimeInterval?
}
