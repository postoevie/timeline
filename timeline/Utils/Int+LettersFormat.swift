//
//  Int+LettersFormat.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

//https://newbedev.com/swift-4-formatting-number-s-into-friendly-k-s

extension Int {

    func formatWithLetters() -> String {
        let num = abs(Double(self))
        let sign = (self < 0) ? "-" : ""

        switch num {
        case 1_000_000_000...:
            let formatted = Int(num / 1_000_000_000)
            return "\(sign)\(formatted)B"

        case 1_000_000...:
            let formatted = Int(num / 1_000_000_)
            return "\(sign)\(formatted)M"

        case 1_000...:
            let formatted = Int(num / 1_000)
            return "\(sign)\(formatted)K"

        case 0...:
            return "\(self)"

        default:
            return "\(sign)\(self)"
        }
    }
}
