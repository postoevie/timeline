//
//  Closures.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

typealias VoidClosure = () -> Void
typealias TimelineDataClosure = ([TimelineCellData]) -> Void
typealias DataClosure = (Data) -> Void
typealias ImageClosure = (UIImage?) -> Void
