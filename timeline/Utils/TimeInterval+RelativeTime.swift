//
//  TimeInterval+NamedFormat.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

extension TimeInterval {
    
    func formatWithRelativeTime() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.dateTimeStyle = .numeric
        return formatter.localizedString(fromTimeInterval: self)
    }
}
