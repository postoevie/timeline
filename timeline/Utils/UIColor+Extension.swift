//
//  UIColor+Extension.swift
//  timeline
//
//  Created by Igor Postoev on 06.01.2022.
//

extension UIColor {
    
    static let xLightGray = #colorLiteral(red: 0.9182453156, green: 0.9182668328, blue: 0.9182552695, alpha: 1)
    static let darkGreen = #colorLiteral(red: 0.1738015115, green: 0.6125526428, blue: 0.2161409259, alpha: 1)
}
