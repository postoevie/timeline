//
//  Image+Scale.swift
//  timeline
//
//  Created by Igor Postoev on 06.01.2022.
//

extension UIImage {
    
    func scaledToFit(width: CGFloat) -> UIImage? {
        guard size.width > 0 else {
            return nil
        }
        let widthRatio = width / size.width
        let newWidth = widthRatio * size.width
        let size = CGSize(width: newWidth, height: size.height * widthRatio)
        let rect = CGRect(origin: .zero, size: size)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
