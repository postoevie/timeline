//
//  TimelineDataSource.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

import SwiftyJSON

class TimelineDataSource {
    
    private let timelineDataFetchService = TimelineFetchService()
    private let imageFetchService = ImageFetchService()
    
    private let basicUrl = URL(string: Constants.Web.osnovaUrlString)!
    
    func fetchData(completion: TimelineDataClosure?) {
        timelineDataFetchService.fetchData { [weak self] data in
            guard let json = try? JSON(data: data) else {
                return
            }
            let items = json["result"]["items"].arrayValue
            let timelineData = items.compactMap { item in
                self?.createTimelineData(with: item)
            }
            completion?(timelineData)
        }
    }
    
    private func createTimelineData(with json: JSON) -> TimelineCellData? {
        guard json["type"].stringValue == "entry" else {
            return nil
        }
        let data = json["data"]
        
        // create header data
        let subsiteData = data["subsite"]
        let authorData = data["author"]
        let creationDate = Date(timeIntervalSince1970: data["date"].doubleValue)
        
        let avatarId = subsiteData["avatar"]["data"]["uuid"].string
        var avatarUrl: URL?
        if let uid = avatarId {
            avatarUrl = basicUrl.appendingPathComponent(uid)
        }
        
        let headerCellData = TimeLineCellHeaderData(avatarUrl: avatarUrl,
                                                    subsiteName: subsiteData["name"].string,
                                                    authorName: authorData["name"].string,
                                                    creationTimeInterval: creationDate.timeIntervalSinceNow)
        // create footer data
        let countersData = data["counters"]
        let footerCellData = TimelineCellFooterData(commentNumber: countersData["comments"].intValue,
                                                    ratingNumber: countersData["favorites"].intValue)
        // create cell data
        let blocks = data["blocks"].arrayValue
        let textData = blocks.first { block in
            return block["type"].stringValue == "text"
        }
        let mediaData = blocks.first { block in
            return block["type"].stringValue == "media"
        }
      
        let mediaItem = mediaData?["data"]["items"].arrayValue.first
        let imageId = mediaItem?["image"]["data"]["uuid"].string
        var imageUrl: URL?
        if let uid = imageId {
            imageUrl = basicUrl.appendingPathComponent(uid)
        }
        let cellData = TimelineCellData(headerData: headerCellData,
                                        imageUrl: imageUrl,
                                        title: data["title"].stringValue,
                                        subtitle: textData?["data"]["text"].string,
                                        footerData: footerCellData)
        return cellData
    }
    
    func fetchImage(url: URL, completion: @escaping ImageClosure) {
        imageFetchService.fetchImage(url: url as NSURL, completion: completion)
    }
}
