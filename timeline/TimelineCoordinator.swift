//
//  TimelineCoordinator.swift
//  timeline
//
//  Created by Igor Postoev on 04.01.2022.
//

class TimelineCoordinator {
    
    private var controller: TimelineViewController?
    private var presenter: TimelinePresenter?
    
    func createController() {
        let presenter = TimelinePresenter()
        let controller = TimelineViewController()
        presenter.fetchingDataViewCompletion = { [weak controller] in
            controller?.setLoading(state: false)
            controller?.reload()
        }
        controller.presenter = presenter
        self.controller = controller
        self.presenter = presenter
    }
    
    func loadData() {
        controller?.setLoading(state: true)
        presenter?.fetchData()
    }
    
    func getController() -> TimelineViewController? {
        return controller
    }
}
